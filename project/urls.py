from django.conf import settings
from django.urls import include, path
from django.contrib import admin

from words.urls import urlpatterns as words_urlpatterns

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # path('', index),
    path('admin/', admin.site.urls),
]

urlpatterns = urlpatterns + words_urlpatterns

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
