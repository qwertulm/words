from django.http import HttpResponse
from django.contrib.auth import login, authenticate, logout, update_session_auth_hash
from django.shortcuts import render, redirect
from words.forms import *
from .models import *
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages

from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def words(request):
    word_list = WordsFilter.filter_by_request(request)
    page = request.GET.get('page', 1)

    paginator = Paginator(word_list, 30)
    try:
        words_qs = paginator.page(page)
    except PageNotAnInteger:
        words_qs = paginator.page(1)
    except EmptyPage:
        words_qs = paginator.page(paginator.num_pages)

    return render(request, "words-list.html", {'words': words_qs})


def words_groups(request):
    words_group_list = WordGroup.objects.all()
    page = request.GET.get('page', 1)

    paginator = Paginator(words_group_list, 30)
    try:
        group_qs = paginator.page(page)
    except PageNotAnInteger:
        group_qs = paginator.page(1)
    except EmptyPage:
        group_qs = paginator.page(paginator.num_pages)

    return render(request, "words-groups-list.html", {'groups': group_qs})


def register_view(request):
    title = "Register"
    if request.user.is_authenticated:
        return redirect('profile_edit')

    form = UserRegistrationForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get("password")
        user.set_password(password)
        user.save()
        login(request, user)
        return redirect('profile_edit')

    return render(request, "forms/signup.html", {'form': form, 'title': title})


def profile(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            word = form.save(commit=False)
            word.save()
            return redirect('profile_edit')

    else:
        user = request.user
        form = UserProfileForm(initial={
            'username': user.username,
            'email': user.email,
        })

    return render(request, "profile_edit.html", {
        'form': form,
        'title': 'Профиль'
    })


def change_password_view(request):
    form = UserChangePasswordForm(request.POST or None, request.user)
    if request.method == 'POST':
        if form.is_valid():
            user = request.user

            user.set_password(form.cleaned_data.get('password_new_1'))
            user.save()

            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')

    return render(request, "profile_change_password.html", {
        'form': form,
        'title': 'Профиль'
    })


def logout_view(request):
    logout(request)
    return redirect('home')


def login_view(request):
    title = "Login"
    if request.user.is_authenticated:
        return redirect('profile_edit')

    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('profile_edit')

    return render(request, "forms/login.html", {'form': form, 'title': title})


def home_view(request):
    return render(request, "home.html", {
        'something_interesting': SomethingInteresting.random()
    })


def create_word_view(request):
    form = WordCreationForm(request.POST or None)
    if form.is_valid():
        word = form.save(commit=False)
        word.save()
        return redirect('words')

    return render(request, "forms/create_word.html", {'form': form, 'title': 'Создать слово'})


def create_words_group(request):
    form = WordsGroupCreationForm(request.POST or None)
    if form.is_valid():
        word = form.save(commit=False)
        word.save()
        return redirect('words_groups')

    return render(request, "forms/create_words_group.html", {'form': form, 'title': 'Создать группу'})


def import_words(request):
    file = open("data/input.txt", "r")

    text = file.read()

    list = text.split("\n")

    output = ''

    for item in list:
        num, frequency, name, type = item.split(' ')
        output += type + '\n'

        word = Word()
        word.num = num
        word.frequency = frequency
        word.name = name
        word.type = type

        word.save()

    file.close()
    return HttpResponse('import')




