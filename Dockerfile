FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/


ENV DATABASE_SERVICE_NAME 'postgresql'
ENV DATABASE_ENGINE 'postgresql'
ENV DATABASE_NAME 'postgres'
ENV DATABASE_USER 'postgres'
ENV DATABASE_PASSWORD ''
ENV POSTGRESQL_SERVICE_HOST 'db'
ENV POSTGRESQL_SERVICE_PORT '5432'