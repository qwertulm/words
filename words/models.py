from django.db import models
import random


class Word(models.Model):
    num = models.IntegerField(blank=True, null=True)
    frequency = models.CharField(max_length=100, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name

    def length(self):
        return len(self.name)


class WordGroup(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    words = models.ManyToManyField("Word", blank=True)

    def __str__(self):
        return self.name

    def length(self):
        return len(self.name)


class SomethingInteresting(models.Model):
    title = models.CharField(max_length=256)
    img_src = models.CharField(max_length=2000)
    link = models.CharField(max_length=2000)

    def __str__(self):
        return self.title

    @staticmethod
    def random():
        items = SomethingInteresting.objects.all()

        if items:
            return random.choice(items)
        else:
            return SomethingInteresting()


class WordsFilter:
    @staticmethod
    def filter_by_request(request):
        """
        :param WSGIRequest request:
        :return:
        """

        word_list = []
        if request.GET.get('group', ''):
            group_id = request.GET.get('group')
            group = WordGroup.objects.get(id=group_id)
            word_list = group.words.all()
        else:
            word_list = Word.objects.all()

        return word_list
