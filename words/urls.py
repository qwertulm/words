from django.urls import path

from words.views import *


urlpatterns = [
    path('', home_view, name='home'),
    path('register/', register_view, name='register'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('profile/', profile, name='profile_edit'),
    path('profile/password', change_password_view, name='profile_change_password'),
    path('words/', words, name='words'),
    path('words-groups/', words_groups, name='words_groups'),
    path('words-groups/create/', create_words_group, name='create_words_group'),
    path('words/create/', create_word_view, name='create_word'),
    path('import_words/', import_words, name='import_words'),
]
