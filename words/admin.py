from django.contrib import admin
from words.models import *

admin.site.register(Word)
admin.site.register(SomethingInteresting)
admin.site.register(WordGroup)
