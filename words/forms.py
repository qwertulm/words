from django import forms
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.models import User
from .models import *


class UserLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        user = authenticate(username=username, password=password)
        if username and password:
            if not user:
                raise forms.ValidationError("Incorrect login or password")
            if not user.is_active:
                raise forms.ValidationError("This user does not longer active.")
            return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegistrationForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label='Confirm password')

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'password2',
        ]

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("This email has been already registered")
        return email

    def clean_password2(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password != password2:
            raise forms.ValidationError("Passwords doesn't match")
        return password


class WordCreationForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Word
        fields = [
            'name',
        ]

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if Word.objects.filter(name=name).exists():
            raise forms.ValidationError("This word has been already created")
        return name


class WordsGroupCreationForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = WordGroup
        fields = [
            'name',
        ]

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if WordGroup.objects.filter(name=name).exists():
            raise forms.ValidationError("This group has been already created")
        return name


class UserProfileForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.instance = kwargs.get('instance', User)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
        ]

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if self.instance.email != email and User.objects.filter(email=email).exists():
            raise forms.ValidationError("This email has been already registered")
        return email


class UserChangePasswordForm(forms.Form):
    password_old = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), required=False)
    password_new_1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), required=False)
    password_new_2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), required=False)

    def __init__(self, request=None, user=None):
        super(UserChangePasswordForm, self).__init__(request)
        self.user = user

    def clean(self, *args, **kwargs):
        password_old = self.cleaned_data.get('password_old')

        if not self.user.check_password(password_old):
            raise forms.ValidationError("Old password not correct")

        password_new_1 = self.cleaned_data.get('password_new_1')
        password_new_2 = self.cleaned_data.get('password_new_2')

        if password_new_1 != password_new_2:
            raise forms.ValidationError("Passwords not match")

        if len(password_new_1) < 5:
            raise forms.ValidationError("New password too shot")

        return super(UserChangePasswordForm, self).clean(*args, **kwargs)


